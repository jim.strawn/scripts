const db = require('@advanced-agrilytics/database');
const config = require('config');
const asyncConfig = require('config/async.js');
const csv = require('csv-stringify/sync');
const fs = require('fs');
const mysql = require('./mysql');

async function go() {
  await asyncConfig.resolveAsyncConfigs(config)
  .catch((err) => {
    console.error('Failed to start application', err);
    throw err;
  });

  const model = await db.getModels().growerOperations;
  const tfsResults = await model.findAll();

  if(tfsResults.length === 0) {
    console.log('No results found');
    return;
  }
  
  console.log('Number of Terraframing Operations', tfsResults.length);

  const sql = 'SELECT uuid, aa_guid, name FROM operation WHERE aa_guid =?;'

  const diffResults = await Promise.all(tfsResults.map(async (result) => {
    const tfsGuid = result.guid;
    const tfsOperationName = result.operationName;
    const farmgateOperation = await mysql.query(sql, tfsGuid);
    if(farmgateOperation.length === 0) { 
      return [tfsGuid, tfsOperationName];
    }
  }));

  const filteredResults = diffResults.filter((result) => result !== undefined);

  console.log('Number of Terraframing Operations not found in Farmgate', filteredResults.length);

  const headers = [
    'terraframing guid', 
    'terraframing operation name',
  ];

  filteredResults.unshift(headers);

  try {
    fs.writeFileSync('operations-diff.csv', csv.stringify(filteredResults));
  } catch (err) {
    console.error(err);
  };
}

go()
  .then(() => {
    console.log('finished.....');
    process.exit(0);
  }).catch((err) => {
    console.error(err);
    process.exit(1);
})

