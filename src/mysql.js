const config = require('config');
const mysql = require('mysql');

let poolSingleton;
const loggingEnabled = false;
function getPool() {
  if (!poolSingleton) {
    poolSingleton = mysql.createPool({
      database: 'crm',
      connectionLimit: 40,
      host: config.mysql.host,
      port: config.mysql.port,
      user: config.mysql.username,
      password: config.mysql.password,
    });
  }

  return poolSingleton;
}

async function row(sql, substitutions) {
  const results = await query(sql, substitutions);
  return results[0];
}

async function queryWithoutErrorLogging(sql, substitutions) {
  const MAX_ATTEMPTS = 5;
  const DELAY_BETWEEN_ATTEMPTS_MS = 100;
  let error;
  let results;

  for (let i = 0; ; await timers.setTimeout(DELAY_BETWEEN_ATTEMPTS_MS)) {
    [error, results] = await new Promise((resolve) => {
      getPool().query(sql, substitutions, (...args) => {
        resolve(args);
      });
    });
    if (!error || error.code != `ER_LOCK_DEADLOCK` || ++i < MAX_ATTEMPTS) break;
  }
  if (error) throw error;
  return results;
}

async function query(sql, substitutions) {
  const start = Date.now();
  sql = sql.replace(/\s{2,}/g, ' ').trim();
  if(loggingEnabled)
    console.log('DB query starting', { sql, substitutions });

  return queryWithoutErrorLogging(sql, substitutions)
    .catch((error) => {
      console.error('DB query failed', { sql, error, substitutions });
      throw error;
    })
    .finally(() => {
      if(loggingEnabled) {
        console.log('loggingEnabled')
        const duration = Date.now() - start;
        console.log('DB query complete', { sql, duration, substitutions });
      }
    });
}

module.exports = {
  query,
  row,
};



